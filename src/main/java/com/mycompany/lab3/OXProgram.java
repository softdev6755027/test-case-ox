/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab3;

/**
 *
 * @author informatics
 */
class OXProgram {

    //Refactor
    static boolean chcekWin(String[][] table, String currentPlayer) {
        for (int row = 0; row < 3; row++) {
            if (CheckRow1(table, currentPlayer, 0)) {
                return true;
            }
            if (CheckRow2(table, currentPlayer, 1)) {
                return true;
            }
            if (CheckRow3(table, currentPlayer, 2)) {
                return true;
            }

        }

        for (int col = 0; col < 3; col++) {

            if (CheckCol1(table, currentPlayer, 0)) {
                return true;
            }
            if (CheckCol2(table, currentPlayer, 1)) {
                return true;
            }
            if (CheckCol3(table, currentPlayer, 2)) {
                return true;
            }
        }

        if (table[0][0].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][2].equals(currentPlayer)) {
            return true;
        } else if (table[0][2].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][0].equals(currentPlayer)) {
            return true;
        }
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (table[row][col] != "X" && table[row][col] != "O") {
                    return false;
                }
            }
        }

        return false;

    }

    private static boolean CheckRow1(String[][] table, String currentPlayer, int row) {
        return table[row][0].equals(currentPlayer) && table[row][1].equals(currentPlayer) && table[row][2].equals(currentPlayer);

    }

    private static boolean CheckRow2(String[][] table, String currentPlayer, int row) {
        return table[row][0].equals(currentPlayer) && table[row][1].equals(currentPlayer) && table[row][2].equals(currentPlayer);
    }

    private static boolean CheckRow3(String[][] table, String currentPlayer, int row) {
        return table[row][0].equals(currentPlayer) && table[row][1].equals(currentPlayer) && table[row][2].equals(currentPlayer);
    }

    private static boolean CheckCol1(String[][] table, String currentPlayer, int col) {
        return table[0][col].equals(currentPlayer) && table[1][col].equals(currentPlayer) && table[2][col].equals(currentPlayer);

    }

    private static boolean CheckCol2(String[][] table, String currentPlayer, int col) {
        return table[0][col].equals(currentPlayer) && table[1][col].equals(currentPlayer) && table[2][col].equals(currentPlayer);

    }

    private static boolean CheckCol3(String[][] table, String currentPlayer, int col) {
        return table[0][col].equals(currentPlayer) && table[1][col].equals(currentPlayer) && table[2][col].equals(currentPlayer);

    }

}
