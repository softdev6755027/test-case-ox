/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class OXProgramUnitTest {
    
    public OXProgramUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
//    @Test
//    public void testCheckWinNoPlayBY_O_output_false() {
//        String [][] table = {{"1","2","3"},{"4","5","6"},{"7","8","9"}} ;
//        String currentPlayer = "O";
//        assertEquals(false, OXProgram.chcekWin(table,currentPlayer));      
//    }
//    
    @Test
    public void testCheckWinRow2BY_O_output_true() {
        String [][] table = {{"1","2","3"},{"O","O","O"},{"7","8","9"}} ;
        String currentPlayer = "O";
        assertEquals(true, OXProgram.chcekWin(table,currentPlayer));       
    }
    
    @Test
    public void testCheckWinRow1BY_X_output_true() {
        String [][] table = {{"X","X","X"},{"4","O","O"},{"-","-","-"}} ;
        String currentPlayer = "X";
        assertEquals(true, OXProgram.chcekWin(table,currentPlayer));       
    }
    
    @Test
    public void testCheckWinRow3BY_X_output_true() {
        String [][] table = {{"-","O","O"},{"-","-","-"},{"X","X","X"}} ;
        String currentPlayer = "X";
        assertEquals(true, OXProgram.chcekWin(table,currentPlayer));       
    }
    
    @Test
    public void testCheckWinCol1BY_X_output_true() {
        String [][] table = {{"X","O","O"},{"X","-","-"},{"X","O","X"}} ;
        String currentPlayer = "X";
        assertEquals(true, OXProgram.chcekWin(table,currentPlayer));       
    }
    
    @Test
    public void testCheckWinCol2BY_X_output_true() {
        String [][] table = {{"O","X","O"},{"O","X","-"},{"-","X","-"}} ;
        String currentPlayer = "X";
        assertEquals(true, OXProgram.chcekWin(table,currentPlayer));       
    }
    
    @Test
    public void testCheckWinCol3BY_X_output_true() {
        String [][] table = {{"O","O","X"},{"-","O","X"},{"-","-","X"}} ;
        String currentPlayer = "X";
        assertEquals(true, OXProgram.chcekWin(table,currentPlayer));       
    }
    
    @Test
    public void testCheckWinDiagonal1BY_X_output_true() {
        String [][] table = {{"X","O","-"},{"-","X","O"},{"-","-","X"}} ;
        String currentPlayer = "X";
        assertEquals(true, OXProgram.chcekWin(table,currentPlayer));       
    }
    
    @Test
    public void testCheckWinDiagonal2BY_X_output_true() {
        String [][] table = {{"O","-","X"},{"-","X","O"},{"X","-","-"}} ;
        String currentPlayer = "X";
        assertEquals(true, OXProgram.chcekWin(table,currentPlayer));       
    }
    
    @Test
    public void testCheckDrawBY_X_output_true() {
        String [][] table = {{"X","O","X"},{"O","X","O"},{"O","X","X"}} ;
        String currentPlayer = "X";
        assertEquals(true, OXProgram.chcekWin(table,currentPlayer));       
    }

   
    
}
